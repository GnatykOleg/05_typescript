import {
  additionalPoints,
  minStudentScore,
} from "../../data/university-data/StudentManagementData";

import { Max } from "../../decorators/common-decorators/commonDecorators";

import {
  IStudentBaseClass,
  Role,
} from "../../types/persons-types/personsTypes";

export default class StudentManagement {
  // Private readonly properties
  @Max(100)
  private readonly _minStudentScore = minStudentScore;

  @Max(10)
  private readonly _additionalPoints = additionalPoints;

  // Private properties
  private _students: IStudentBaseClass[] = [];

  @Max(120)
  private _totalStudentScore: number = 0;

  // Getters
  public get students() {
    return this._students;
  }

  // Private methods
  private _addAdditionalPoints(): void {
    this._totalStudentScore += this._additionalPoints;
  }

  private findStudent(id: string): IStudentBaseClass | undefined {
    return this._students.find((student) => student.id === id);
  }

  // Public methods
  public addStudent<S extends IStudentBaseClass>(newStudent: S): void {
    const {
      overallExamsScore,
      participationInTheOlympiads,
      coveringLetter,
      id,
      role,
    } = newStudent;

    if (role !== Role.Student) return console.error("You are not a student");

    const studentFullName = newStudent.getStudentFullName();

    const findStudent = this.findStudent(id);

    const studentAlredyExistsMessage = `\nStudent ${studentFullName} is already study at our university`;

    if (findStudent) return console.error(studentAlredyExistsMessage);

    this._totalStudentScore = overallExamsScore;

    if (participationInTheOlympiads) this._addAdditionalPoints();

    if (coveringLetter) this._addAdditionalPoints();

    const lowScoreMessage = `\n${studentFullName} you did not score the required number of points: ${this._totalStudentScore}, for admission to our university you need at least ${this._minStudentScore}.`;

    if (this._totalStudentScore < this._minStudentScore)
      return console.error(lowScoreMessage);

    // Successful add student
    const successfulAddStudent = `\nCongratulations ${studentFullName} now you are part of our university`;

    console.log(successfulAddStudent);

    this._students.push(newStudent);
  }

  public removeStudent<S extends IStudentBaseClass>(studentToRemove: S) {
    const { id } = studentToRemove;

    const studentFullName = studentToRemove.getStudentFullName();

    const findStudent = this.findStudent(id);

    const studentDoesntExistsMessage = `\nStudent ${studentFullName} doesnt study at our university`;

    if (!findStudent) return console.error(studentDoesntExistsMessage);

    const successfulRemove = `\n${studentFullName} you are no longer part of our university.`;

    console.log(successfulRemove);

    this._students = this._students.filter((student) => student.id !== id);
  }
}
