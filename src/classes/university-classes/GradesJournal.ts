import { StringParamToTrim } from "../../decorators/common-decorators/commonDecorators";

import { AddGradeProps } from "../../types/persons-types/personsTypes";

import {
  IGradesJournal,
  IJournal,
} from "../../types/university-types/universityTypes";

export default class GradesJournal implements IGradesJournal {
  // Private properties
  private _journal: IJournal[] = [];

  // Getters
  get journal(): IJournal[] {
    return this._journal;
  }

  // Public methods
  private findStudentInJournal(
    @StringParamToTrim studentId: string
  ): IJournal | undefined {
    const student = this._journal.find(({ id }) => id === studentId);

    return student;
  }

  public getGradesForStudent(@StringParamToTrim id: string): IJournal | void {
    const studentGrades = this.findStudentInJournal(id);

    if (!studentGrades)
      return console.error("\nI don't find you in my journal");

    return studentGrades;
  }

  public addGrade({ grade, course, student }: AddGradeProps): void {
    const fullName = student.getStudentFullName();

    const getNewGradeMessage = `\nStudent ${fullName} receives a grade of ${grade} for couser ${course}.`;

    console.log(getNewGradeMessage);

    const date = new Date().toLocaleDateString();

    const gradeWithDate = `grade: '${grade}' add at ${date}`;

    const courseToLowerCase = course.toLowerCase();

    const findStudentInJournal = this.findStudentInJournal(student.id);

    if (!findStudentInJournal) {
      const newStudent = {
        id: student.id,
        fullName,
        [courseToLowerCase]: [gradeWithDate],
      };

      this._journal.push(newStudent);
    }

    const gradesArray = findStudentInJournal?.[courseToLowerCase];

    if (Array.isArray(gradesArray)) gradesArray.push(gradeWithDate);
  }
}
