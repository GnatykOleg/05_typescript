import { minSalaryForRole } from "../../data/university-data/hRDepartmentData";

import { CheckEmployeeRequirements } from "../../decorators/university-decorators/universityDecorators";

import { ReadOnlyDynamicObject } from "../../types/common-types/commonTypes";

import {
  IEmployeeBaseClass,
  Role,
} from "../../types/persons-types/personsTypes";

export default class HRDepartment {
  // Private  properties
  private _employees: IEmployeeBaseClass[] = [];
  private _minSalaryForRole: ReadOnlyDynamicObject<number> = minSalaryForRole;

  // Getters
  public get employees() {
    return this._employees;
  }

  // Private methods
  private _findEmployee<T>({ id, fullName }: { id: T; fullName: T }) {
    const employeeAlredyExistsMessage = `\nEmployee ${fullName} is already working at our university`;

    const employeeDoesntExistsMessage = `\nEmployee ${fullName} doesnt working at our university`;

    const employeeToFind = this._employees.find(
      (employee) => employee.id === id
    );

    const message = employeeToFind
      ? employeeAlredyExistsMessage
      : employeeDoesntExistsMessage;

    const employee = employeeToFind || false;

    return { employee, message };
  }

  private _setSalary(role: Role, yearsOfExperience: number): number {
    const minSalaryForRole = this._minSalaryForRole[role];

    const coefficient = 200;

    const incrementSalaryByExp =
      minSalaryForRole + coefficient * yearsOfExperience;

    return incrementSalaryByExp;
  }

  // Public methods
  @CheckEmployeeRequirements
  public hireEmployee<E extends IEmployeeBaseClass>(newEmployee: E): void {
    const { role, id, yearsOfExperience } = newEmployee;

    const fullName = newEmployee.getEmployeeFullName();

    const { employee, message } = this._findEmployee<string>({ id, fullName });

    if (employee) return console.error(message);

    const successfulHire = `\nCongratulations, ${fullName} you have been hired for a position "${role}" at our university`;

    console.log(successfulHire);

    const salary = this._setSalary(role, yearsOfExperience);

    newEmployee.setSalary({ salary, access: true });

    this._employees.push(newEmployee);
  }

  public fireEmployee<E extends IEmployeeBaseClass>(employeeToRemove: E) {
    const { id } = employeeToRemove;

    const fullName = employeeToRemove.getEmployeeFullName();

    const { employee, message } = this._findEmployee<string>({ id, fullName });

    if (!employee) return console.error(message);

    const successfulFire = `\n${fullName} you are no longer part of our university.`;

    console.log(successfulFire);

    this._employees = this._employees.filter((employee) => employee.id !== id);
  }
}
