import {
  CreatedAt,
  ReadOnlyMethod,
  GetterToStringWithEmoji,
} from "../../decorators/common-decorators/commonDecorators";

import {
  IEmployeeBaseClass,
  IStudentBaseClass,
} from "../../types/persons-types/personsTypes";

import {
  IUniversityLocation,
  IUniversityData,
} from "../../types/university-types/universityTypes";

import HRDepartment from "./HRDepartment";

import StudentManagement from "./StudentManagement";

@CreatedAt
export default class University {
  // Private readonly properties
  private readonly _hrDepartment: HRDepartment = new HRDepartment();
  private readonly _studentManagement: StudentManagement =
    new StudentManagement();

  // Private properties
  private _name: string;
  private _location: IUniversityLocation;
  private _courses: string[];

  constructor({ name, location, courses }: IUniversityData) {
    this._name = name;
    this._location = location;
    this._courses = courses;
  }

  // Getters
  @GetterToStringWithEmoji("🎓")
  get name(): string {
    return this._name;
  }

  @GetterToStringWithEmoji("📚")
  get courses(): string[] {
    return this._courses;
  }

  @GetterToStringWithEmoji("📚")
  get location(): IUniversityLocation {
    return this._location;
  }

  // Setters
  set name(newUniversityName: string) {
    this._name = newUniversityName;
  }

  set location(newLocation: IUniversityLocation) {
    this._location = newLocation;
  }

  // Public methods

  // Employees
  @ReadOnlyMethod
  public getEmployees(): IEmployeeBaseClass[] {
    return this._hrDepartment.employees;
  }

  @ReadOnlyMethod
  public addEmployee<E extends IEmployeeBaseClass>(newEmployee: E): void {
    this._hrDepartment.hireEmployee(newEmployee);
  }

  @ReadOnlyMethod
  public removeEmployee<E extends IEmployeeBaseClass>(
    employeeToRemove: E
  ): void {
    this._hrDepartment.fireEmployee(employeeToRemove);
  }

  // Students
  @ReadOnlyMethod
  public getStudents(): IStudentBaseClass[] {
    return this._studentManagement.students;
  }

  @ReadOnlyMethod
  public addStudent<S extends IStudentBaseClass>(newStudent: S): void {
    this._studentManagement.addStudent(newStudent);
  }

  @ReadOnlyMethod
  public removeStudent<S extends IStudentBaseClass>(studentToRemove: S): void {
    this._studentManagement.removeStudent(studentToRemove);
  }
}
