import { CreatedAt } from "../../../decorators/common-decorators/commonDecorators";

import {
  GiveBrabeProps,
  IStudentData,
  ProductivityLevel,
  Role,
} from "../../../types/persons-types/personsTypes";

import StudentBase from "../base-classes/StudentBase";

@CreatedAt
export default class Student extends StudentBase {
  constructor(studentData: IStudentData) {
    super({ ...studentData, role: Role.Student });
  }

  // Public methods
  public giveBribe({ teacher, amount }: GiveBrabeProps): void {
    const giveBribeMessage = `\nI'm offering you a gift ${amount}$ in exchange for skipping class.`;

    console.log(giveBribeMessage);

    teacher.receiveBribe(amount);
  }

  public getStudentFullInfo() {
    let info = "\nInformation about a person:\n";

    for (const prop in this) {
      const isPrivateProp = prop.startsWith("_");

      const propName = isPrivateProp ? prop.slice(1) : prop;

      info += `${propName}: ${this[prop]}\n`;
    }

    return info;
  }

  public getStudentProductivityStatus(): ProductivityLevel {
    return this._productivity;
  }
}
