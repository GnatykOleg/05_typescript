import { v4 as uuidv4 } from "uuid";

import {
  GenderType,
  IStudentBaseClass,
  IStudentBaseConstructorParams,
  ProductivityLevel,
  Role,
} from "../../../types/persons-types/personsTypes";

export default abstract class StudentBase implements IStudentBaseClass {
  // Protected  properties
  protected _productivity: ProductivityLevel = "medium";

  // Private readonly properties
  private readonly _id: string = uuidv4();
  private readonly _role: Role;

  private readonly _overallExamsScore: number;

  // Private properties
  private _coveringLetter: boolean;
  private _participationInTheOlympiads: boolean;

  // Public properties
  public firstName: string;
  public lastName: string;
  public gender: GenderType;
  public age: number;

  constructor(student: IStudentBaseConstructorParams) {
    if (student.overallExamsScore > 100)
      throw new Error(`Max overall exams score is 100`);

    this._coveringLetter = student.coveringLetter;
    this._participationInTheOlympiads = student.participationInTheOlympiads;
    this._overallExamsScore = student.overallExamsScore;
    this._role = student.role;

    this.firstName = student.firstName;
    this.lastName = student.lastName;
    this.gender = student.gender;
    this.age = student.age;
  }

  // Getters
  get id(): string {
    return this._id;
  }

  get role(): Role {
    return this._role;
  }

  get overallExamsScore() {
    return this._overallExamsScore;
  }

  get coveringLetter() {
    return this._coveringLetter;
  }

  get participationInTheOlympiads() {
    return this._participationInTheOlympiads;
  }

  // Setters
  set coveringLetter(flag: boolean) {
    this._coveringLetter = flag;
  }

  set participationInTheOlympiads(flag: boolean) {
    this._participationInTheOlympiads = flag;
  }

  // Public methods
  public getStudentFullName(): string {
    return `${this.firstName} ${this.lastName}`;
  }

  // Abstracts methods
  abstract getStudentFullInfo(): string;

  abstract getStudentProductivityStatus(): string;
}
