import { v4 as uuidv4 } from "uuid";

import {
  IEmployeeBaseClass,
  GenderType,
  IEmployeeBaseConstructorParams,
  Role,
  SetSalaryProps,
  ProductivityLevel,
} from "../../../types/persons-types/personsTypes";

import { LogSetter } from "../../../decorators/common-decorators/commonDecorators";

export default abstract class EmployeeBase implements IEmployeeBaseClass {
  // Protected  properties
  protected _productivity: ProductivityLevel = "medium";

  // Private readonly properties
  private readonly _id: string = uuidv4();
  private readonly _role: Role;

  // Private properties
  private _salary: number = 0;
  private _specialization: string;
  private _yearsOfExperience: number;

  // Public properties
  public firstName: string;
  public lastName: string;
  public gender: GenderType;
  public age: number;

  constructor(employee: IEmployeeBaseConstructorParams) {
    this._yearsOfExperience = employee.yearsOfExperience;
    this._specialization = employee.specialization;
    this._role = employee.role;

    this.firstName = employee.firstName;
    this.lastName = employee.lastName;
    this.gender = employee.gender;
    this.age = employee.age;
  }

  // Getters
  get id(): string {
    return this._id;
  }

  get role(): Role {
    return this._role;
  }

  get salary(): number {
    return this._salary;
  }

  get specialization(): string {
    return this._specialization;
  }

  get yearsOfExperience(): number {
    return this._yearsOfExperience;
  }

  // Setters
  @LogSetter
  set yearsOfExperience(newYearsOfExperience: number) {
    this._yearsOfExperience = newYearsOfExperience;
  }

  @LogSetter
  set specialization(newSpecialization: string) {
    this._specialization = newSpecialization;
  }

  // Public methods
  public setSalary({ salary, access }: SetSalaryProps): number | void {
    // Here I simulated access to a salary change, since the employee himself should not be able to change it
    const errorMessage = "\nYou cannot change salary without access";

    if (!access) return console.error(errorMessage);

    this._salary = salary;
  }

  public getEmployeeFullName(): string {
    return `${this.firstName} ${this.lastName}`;
  }

  // Abstracts methods
  abstract getEmployeeFullInfo(): string;

  abstract getEmployeeProductivityStatus(): string;
}
