import GradesJournal from "../../university-classes/GradesJournal";

import EmployeeBase from "../base-classes/EmployeeBase";

import { IJournal } from "../../../types/university-types/universityTypes";

import {
  AddGradeProps,
  IEmployeeData,
  ProductivityLevel,
  Role,
} from "../../../types/persons-types/personsTypes";

import { CreatedAt } from "../../../decorators/common-decorators/commonDecorators";

@CreatedAt
export default class Teacher extends EmployeeBase {
  private readonly _gradesJournal: GradesJournal = new GradesJournal();

  constructor(employeeData: IEmployeeData) {
    super({ ...employeeData, role: Role.Teacher });
  }

  // Getters
  get gradesJournal() {
    return this._gradesJournal;
  }

  // Public methods
  public getGradesForStudent(id: string): void | IJournal {
    return this._gradesJournal.getGradesForStudent(id);
  }

  public addGrade({ grade, course, student }: AddGradeProps): void {
    this._gradesJournal.addGrade({ grade, course, student });
  }

  public receiveBribe(bribe: number): never {
    const refuseBribe = `\nYou just offered me a bribe in the ${bribe}$, but you're out of luck, I don't take them.`;

    throw new Error(refuseBribe);
  }

  public getEmployeeFullInfo() {
    let info = "\nInformation about a person:\n";

    for (const prop in this) {
      const isPrivateProp = prop.startsWith("_");

      const propName = isPrivateProp ? prop.slice(1) : prop;

      const isPropValueObject = typeof this[prop] === "object";

      const propValue = isPropValueObject
        ? JSON.stringify(this[prop], null, 2)
        : this[prop];

      info += `${propName}: ${propValue}\n`;
    }

    return info;
  }

  public getEmployeeProductivityStatus(): ProductivityLevel {
    return this._productivity;
  }
}
