import { CreatedAt } from "../../../decorators/common-decorators/commonDecorators";

import {
  AwardsFieldInObject,
  IDirectorAdditionalInfo,
  IEmployeeData,
  ProductivityLevel,
  Role,
} from "../../../types/persons-types/personsTypes";

import EmployeeBase from "../base-classes/EmployeeBase";

@CreatedAt
export default class Director extends EmployeeBase {
  // Private properties
  private _additionalInformation: Partial<IDirectorAdditionalInfo> = {};

  constructor(employeeData: IEmployeeData) {
    super({ ...employeeData, role: Role.Director });
  }

  // Getters
  get additionalInformation() {
    return this._additionalInformation;
  }

  // Public statis methods
  static isDirector<D>(employee: D): employee is D {
    return employee instanceof Director;
  }

  // Public methods us key in
  public hasAwards(): boolean {
    return "awards" in this._additionalInformation;
  }

  public addAward(award: string): void {
    const date = new Date().toLocaleDateString();

    if (!this.hasAwards())
      this._additionalInformation.awards = [] as Array<AwardsFieldInObject>;

    this._additionalInformation.awards?.push({ awardName: award, date });
  }

  public getAwardByName(name: string): AwardsFieldInObject | void {
    const findAward = this._additionalInformation.awards?.find(
      ({ awardName }) => awardName == name
    );

    return findAward || console.log(`\nAward: ${name} does not exist`);
  }

  public getEmployeeFullInfo() {
    let info = "\nInformation about a person:\n";

    for (const prop in this) {
      const isPrivateProp = prop.startsWith("_");

      const propName = isPrivateProp ? prop.slice(1) : prop;

      const isPropValueObject = typeof this[prop] === "object";

      const propValue = isPropValueObject
        ? JSON.stringify(this[prop], null, 2)
        : this[prop];

      info += `${propName}: ${propValue}\n`;
    }

    return info;
  }

  public getEmployeeProductivityStatus(): ProductivityLevel {
    return this._productivity;
  }
}
