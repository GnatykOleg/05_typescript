import EmployeeBase from "../../classes/persons-classes/base-classes/EmployeeBase";
import {
  minYearsExperienceForRole,
  specializationsByRole,
} from "../../data/university-data/hRDepartmentData";

export function CheckEmployeeRequirements(
  target: Object,
  propertyKey: PropertyKey,
  descriptor: PropertyDescriptor
) {
  const originalMethod = descriptor.value;

  descriptor.value = function (...args: any[]) {
    // Checks at least 1 of EmployeeBase instance arguments
    const employee = args.find((arg) => arg instanceof EmployeeBase);

    // There is no required argument, we throw an error
    const errorMessage =
      "The 'CheckEmployeeRoleAvailability' decorator can only be applied to methods that accept at least one argument of type 'EmployeeBase' or its derived classes.";
    if (!employee) throw new Error(errorMessage);

    // Declaring the required data with a type assertion
    const { role, specialization, yearsOfExperience } =
      employee as EmployeeBase;
    const fullName = employee.getEmployeeFullName();

    // We are looking for specialization for the role of a new employee
    const roleSpecialization = specializationsByRole[role];

    // The university does not have a list of specializations for this role.
    const missingRoleMessage = `\nFor ${fullName}, we do not have any positions available for the role '${role}' at our university`;
    if (!roleSpecialization) return console.error(missingRoleMessage);

    // There are specializations for this role, but the employee does not have the required specialization
    const wrongSpecializationMessage = `\n${fullName}, sorry but your specialization '${specialization}' does not meet our requirements.`;
    if (!roleSpecialization.includes(specialization.toLowerCase()))
      return console.error(wrongSpecializationMessage);

    // We get minimum years of experience for specialization
    const roleMinYearsExperience = minYearsExperienceForRole[role];

    // The employee has little experience for this role
    const littleExperienceMessage = `\n${fullName} to work '${role}' requires a minimum of "${roleMinYearsExperience}" years of experience`;
    if (yearsOfExperience < roleMinYearsExperience)
      return console.error(littleExperienceMessage);

    return originalMethod.apply(this, args);
  };

  return descriptor;
}
