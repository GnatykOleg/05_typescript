export function CreatedAt<T extends { new (...args: any[]): {} }>(
  constructor: T
) {
  return class extends constructor {
    public createdAt = new Date();
  };
}

export function ReadOnlyMethod(
  target: Object,
  propertyKey: PropertyKey,
  descriptor: PropertyDescriptor
) {
  descriptor.writable = false;
  return descriptor;
}

export function Max(max: number) {
  return (target: Object, propertyKey: PropertyKey) => {
    let value: number;

    const setter = function (newValue: number) {
      const key = String(propertyKey);

      const errorMessage = `Value ${key} cannot be greater than ${max}`;

      if (newValue > max) throw new Error(errorMessage);

      value = newValue;
    };

    const getter = function () {
      return value;
    };

    Object.defineProperty(target, propertyKey, {
      set: setter,
      get: getter,
      enumerable: true,
      configurable: true,
    });
  };
}

export function GetterToStringWithEmoji(emoji: string) {
  return function (
    target: Object,
    propertyKey: PropertyKey,
    descriptor: PropertyDescriptor
  ) {
    const originalGetter = descriptor.get;

    if (!originalGetter)
      throw new Error(
        `GetterToStringWithEmoji decorator can only be used with a getter.`
      );

    descriptor.get = function (this: any) {
      const value = originalGetter.call(this);

      const isValueArray = Array.isArray(value);

      const isNumber = (item: any) => typeof item === "number";
      const isString = (item: any) => typeof item === "string";
      const isObject = (item: any) => typeof item === "object";
      const isBoolean = (item: any) => typeof item === "boolean";

      const objToString = (item: object) => JSON.stringify(item, null, 2);

      const isNumbersOrStringsOrBooleansTypes =
        isNumber(value) || isString(value) || isBoolean(value);

      if (isNumbersOrStringsOrBooleansTypes) return `${emoji} ${value}`;

      if (isObject(value)) return `${emoji} ${objToString(value)}`;

      const isNumbersOrStringsOrBooleansArray =
        isValueArray &&
        value.every(
          (item) => isNumber(item) || isString(item) || isBoolean(value)
        );

      if (isNumbersOrStringsOrBooleansArray)
        return `${emoji} ${value.join(", ")}`;

      const isObjectsArray =
        isValueArray && value.every((item) => isObject(item));

      if (isObjectsArray) {
        const transformedArray = value.map((item) => objToString(item));

        return `${emoji} ${transformedArray.join(", ")}`;
      }

      const validTypesMessage = `GetterToStringWithEmoji decorator can only be applied to a getter that returns a number, string, boolean, object, array of numbers, array of strings, array of booleans, or array of objects.`;

      throw new Error(validTypesMessage);
    };

    return descriptor;
  };
}

export function LogSetter(
  target: Object,
  propertyKey: PropertyKey,
  descriptor: PropertyDescriptor
): void {
  const set = descriptor.set;

  const key = String(propertyKey);

  descriptor.set = function (...args: any): void {
    const newValue = args.join();

    console.log(`\nYou changed the value of ${key} to '${newValue}'`);

    set?.apply(target, [args]);
  };
}

export function StringParamToTrim(
  target: any,
  propertyKey: PropertyKey,
  parameterIndex: number
) {
  const originalMethod = target[propertyKey];

  target[propertyKey] = function (...args: any[]) {
    let value = args[parameterIndex];

    if (typeof value === "string") value = value.trim();

    return originalMethod.apply(this, args);
  };
}
