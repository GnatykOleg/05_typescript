import {
  harvardUniversity,
  tomasSmithDirector,
  haroldEricksonTeacher,
  helenaRichardsTeacher,
  oliverWilliamsTeacher,
  semKholodovTeacher,
  charlieBoolStudent,
  emilyStackStudent,
  josephLoopStudent,
  lanaKicksStudent,
} from "./instances/instances";

import Director from "./classes/persons-classes/employees/Director";
import Teacher from "./classes/persons-classes/employees/Teacher";
import Student from "./classes/persons-classes/students/Student";

// UNIVERSITY

// Getters with Decorator @GetterToStringWithEmoji
console.log("Harvard University name : ", harvardUniversity.name);
console.log("Harvard University courses : ", harvardUniversity.courses);
console.log("Harvard University location : ", harvardUniversity.location);

// Setters
harvardUniversity.name = "Harvard University";
harvardUniversity.location = {
  address: "25 Stone Avenu",
  city: "Cambridge, Massachusetts",
  country: "UNITED STATES OF AMERICA",
};

// Work with employees methods:

// Hiring director
harvardUniversity.addEmployee<Director>(tomasSmithDirector);

// Checking hiring duplicates
harvardUniversity.addEmployee<Director>(tomasSmithDirector);

// Trying to reassign a method that returns nothing to return a number
// And we get an error because we use the @ReadOnlyMethod decorator
// commented out because it throws an error
// harvardUniversity.addEmployee = () => {
//   return 10;
// };

// Hiring teachers
harvardUniversity.addEmployee<Teacher>(helenaRichardsTeacher);
harvardUniversity.addEmployee<Teacher>(oliverWilliamsTeacher);
harvardUniversity.addEmployee<Teacher>(haroldEricksonTeacher);
harvardUniversity.addEmployee<Teacher>(semKholodovTeacher);

// Remove teacher
harvardUniversity.removeEmployee<Teacher>(haroldEricksonTeacher);

// Checking remove duplicates
harvardUniversity.removeEmployee<Teacher>(haroldEricksonTeacher);

// Get employees list
console.log("\nGet all employees: ", harvardUniversity.getEmployees());

// Work with students methods:

// Add students
harvardUniversity.addStudent<Student>(charlieBoolStudent);
harvardUniversity.addStudent<Student>(emilyStackStudent);
harvardUniversity.addStudent<Student>(josephLoopStudent);
harvardUniversity.addStudent<Student>(lanaKicksStudent);

// Checking add students duplicates
harvardUniversity.addStudent<Student>(lanaKicksStudent);

// Remove students
harvardUniversity.removeStudent<Student>(lanaKicksStudent);

// Checking remove duplicates
harvardUniversity.removeStudent<Student>(lanaKicksStudent);

// Get students list
console.log("\nGet all students: ", harvardUniversity.getStudents());

// EMPLOYEE BASE

// Employee base public properties
console.log("\nTomas Smith Director firstName: ", tomasSmithDirector.firstName);
console.log("\nTomas Smith Director lastName: ", tomasSmithDirector.lastName);
console.log("\nTomas Smith Director gender: ", tomasSmithDirector.gender);
console.log("\nTomas Smith Director age: ", tomasSmithDirector.age);

//We refer to the property that was added using the @CreatedAt class decorator
console.log("\nTomas Smith Director createdAt: ", tomasSmithDirector.createdAt);

// Employee base getters
console.log("\nTomas Smith Director id: ", tomasSmithDirector.id);
console.log("\nTomas Smith Director role: ", tomasSmithDirector.role);
console.log("\nTomas Smith Director salary: ", tomasSmithDirector.salary);
console.log(
  "\nTomas Smith Director specialization: ",
  tomasSmithDirector.specialization
);
console.log(
  "\nTomas Smith Director yearsOfExperience: ",
  tomasSmithDirector.yearsOfExperience
);

// Employee base setters wit Decorator @LogSetter
tomasSmithDirector.yearsOfExperience = 30;
tomasSmithDirector.specialization = "Administration";

// Employee public methods
tomasSmithDirector.setSalary({ access: false, salary: 500 });

console.log(
  "\nTomas Smith Director getEmployeeFullName(): ",
  tomasSmithDirector.getEmployeeFullName()
);

console.log(
  "\nTomas Smith Director getEmployeeFullInfo(): ",
  tomasSmithDirector.getEmployeeFullInfo()
);

console.log(
  "\nTomas Smith Director getEmployeeProductivityStatus(): ",
  tomasSmithDirector.getEmployeeProductivityStatus()
);

// STUDENTS BASE

// Students base public properties
console.log("\nCharlie Bool Student firstName: ", charlieBoolStudent.firstName);
console.log("\nCharlie Bool Student lastName: ", charlieBoolStudent.lastName);
console.log("\nCharlie Bool Student gender: ", charlieBoolStudent.gender);
console.log("\nCharlie Bool Student age: ", charlieBoolStudent.age);

//We refer to the property that was added using the @CreatedAt class decorator
console.log("\nCharlie Bool Student createdAt: ", charlieBoolStudent.createdAt);

// Students base getters with Decorator @GetterToStringWithEmoji
console.log("\nCharlie Bool Student id: ", charlieBoolStudent.id);
console.log("\nCharlie Bool Student role: ", charlieBoolStudent.role);
console.log(
  "\nCharlie Bool Student overallExamsScore: ",
  charlieBoolStudent.overallExamsScore
);
console.log(
  "\nCharlie Bool Student coveringLetter: ",
  charlieBoolStudent.coveringLetter
);
console.log(
  "\nCharlie Bool Student participationInTheOlympiads: ",
  charlieBoolStudent.participationInTheOlympiads
);

// Students base setters
charlieBoolStudent.coveringLetter = true;
charlieBoolStudent.participationInTheOlympiads = true;

// Students public methods
console.log(
  "\nCharlie Bool Student getStudentFullName(): ",
  charlieBoolStudent.getStudentFullName()
);

console.log(
  "\nCharlie Bool Student getStudentFullInfo(): ",
  charlieBoolStudent.getStudentFullInfo()
);

console.log(
  "\nCharlie Bool Student getStudentProductivityStatus(): ",
  charlieBoolStudent.getStudentProductivityStatus()
);

// DIRECTOR
// Getters
console.log(
  "\nGet Tomas Smith Director additional information: ",
  tomasSmithDirector.additionalInformation
);

// Director class static method isDirector
console.log(
  "\nIs Tomas Smith Director a director? ",
  Director.isDirector(tomasSmithDirector)
);

console.log(
  "\nIs Charlie Bool Student a director? ",
  Director.isDirector(charlieBoolStudent)
);

console.log(
  "\nIs Sem Kholodov Teacher a director? ",
  Director.isDirector(semKholodovTeacher)
);

// Add award method for use Partial<> for futures awards
tomasSmithDirector.addAward("Education Leader");
tomasSmithDirector.addAward("Inspiration and Achievement");
tomasSmithDirector.addAward("Cool Director");

// Get director award by name
console.log(
  "\nGet director award by name",
  tomasSmithDirector.getAwardByName("Education Leader")
);

// Check director has awards?
console.log("\nHas director awards?", tomasSmithDirector.hasAwards());

// Check added awards
console.log("\nCheck added awards ", tomasSmithDirector.additionalInformation);

// TEACHER

// Add grades
semKholodovTeacher.addGrade({
  course: semKholodovTeacher.specialization,
  grade: 80,
  student: charlieBoolStudent,
});

semKholodovTeacher.addGrade({
  course: semKholodovTeacher.specialization,
  grade: 40,
  student: charlieBoolStudent,
});

semKholodovTeacher.addGrade({
  course: semKholodovTeacher.specialization,
  grade: 55,
  student: josephLoopStudent,
});

console.log(
  "\nGet grades for student Joseph Loop :",
  semKholodovTeacher.getGradesForStudent(josephLoopStudent.id)
);

console.log(
  "\nGet grades for student Charlie Bool: ",
  semKholodovTeacher.getGradesForStudent(charlieBoolStudent.id)
);

// STUDENT

// Give bribe method throws an error so commented out
// josephLoopStudent.giveBribe({ amount: 500, teacher: semKholodovTeacher });
