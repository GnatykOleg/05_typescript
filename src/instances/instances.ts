import University from "../classes/university-classes/University";
import Teacher from "../classes/persons-classes/employees/Teacher";
import Director from "../classes/persons-classes/employees/Director";
import Student from "../classes/persons-classes/students/Student";

import * as teachersData from "../data/persons-data/teachersData";
import * as directorsData from "../data/persons-data/directorsData";
import * as universityData from "../data/university-data/universityData";
import * as studentsData from "../data/persons-data/studetsData";

import { WithCreatedAt } from "../types/common-types/commonTypes";

// Create university
export const harvardUniversity = new University(
  universityData.harvardUniversity
) as WithCreatedAt<University>;

// Create director
export const tomasSmithDirector = new Director(
  directorsData.tomasSmith
) as WithCreatedAt<Director>;

// Create teachers
export const haroldEricksonTeacher = new Teacher(
  teachersData.haroldErickson
) as WithCreatedAt<Teacher>;

export const helenaRichardsTeacher = new Teacher(
  teachersData.helenaRichards
) as WithCreatedAt<Teacher>;

export const oliverWilliamsTeacher = new Teacher(
  teachersData.oliverWilliams
) as WithCreatedAt<Teacher>;

export const semKholodovTeacher = new Teacher(
  teachersData.semKholodov
) as WithCreatedAt<Teacher>;

// Create students
export const charlieBoolStudent = new Student(
  studentsData.charlieBool
) as WithCreatedAt<Student>;

export const emilyStackStudent = new Student(
  studentsData.emilyStack
) as WithCreatedAt<Student>;

export const josephLoopStudent = new Student(
  studentsData.josephLoop
) as WithCreatedAt<Student>;

export const lanaKicksStudent = new Student(
  studentsData.lanaKicks
) as WithCreatedAt<Student>;
