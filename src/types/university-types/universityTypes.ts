import { AddGradeProps } from "../persons-types/personsTypes";

export interface IUniversityLocation {
  country: string;
  city: string;
  address: string;
}

export interface IUniversityData {
  name: string;
  courses: string[];
  location: IUniversityLocation;
}

export interface IJournal {
  id: string;
  fullName: string;
  [key: string]: string[] | string;
}

export interface IGradesJournal {
  addGrade({ grade, course, student }: AddGradeProps): void;

  getGradesForStudent(id: string): IJournal | void;

  journal: IJournal[];
}
