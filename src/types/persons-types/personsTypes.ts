import Teacher from "../../classes/persons-classes/employees/Teacher";

import Student from "../../classes/persons-classes/students/Student";

// Enums
export enum Role {
  Director = "DIRECTOR",
  Teacher = "TEACHER",

  Student = "STUDENT",
}

// Base person properties
interface BasePersonProperties {
  firstName: string;
  lastName: string;
  gender: GenderType;
  age: number;
}

// EMPLOYEE
export type ReadonlyEmployeeData = Readonly<IEmployeeData>;

export interface IEmployeeData extends BasePersonProperties {
  specialization: string;
  yearsOfExperience: number;
}

export interface IEmployeeBaseClass extends IEmployeeData {
  id: string;

  role: Role;

  salary: number;

  setSalary({ salary, access }: SetSalaryProps): number | void;

  getEmployeeFullName(): string;

  getEmployeeFullInfo(): string;

  getEmployeeProductivityStatus(): string;

  createdAt?: Date;
}

export type IEmployeeBaseConstructorParams = IEmployeeData & { role: Role };

// STUDENT
export type ReadonlyStudentData = Readonly<IStudentData>;

export interface IStudentData extends BasePersonProperties {
  coveringLetter: boolean;
  participationInTheOlympiads: boolean;
  overallExamsScore: number;
}

export interface IStudentBaseClass extends IStudentData {
  id: string;

  role: Role;

  getStudentFullName(): string;

  getStudentFullInfo(): string;

  getStudentProductivityStatus(): string;

  createdAt?: Date;
}

export type IStudentBaseConstructorParams = IStudentData & { role: Role };

// OTHER TYPES
export type SetSalaryProps = { salary: number; access: boolean };

export type AwardsFieldInObject = {
  awardName: string;
  date: string;
};

export interface IDirectorAdditionalInfo {
  awards: Array<AwardsFieldInObject>;
}

export type GenderType = "male" | "female";

export type ProductivityLevel = "low" | "medium" | "high";

export type AddGradeProps = {
  course: string;
  grade: number;
  student: Student;
};

export type GiveBrabeProps = { teacher: Teacher; amount: number };
