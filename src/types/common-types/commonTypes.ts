export type DynamicObject<T> = { [key: string]: T };

export type ReadOnlyDynamicObject<T> = Readonly<{ [key: string]: T }>;

export type WithCreatedAt<T> = T & { createdAt: Date };

export type PropertyKey = string | symbol;
