import { ReadonlyEmployeeData } from "../../types/persons-types/personsTypes";

export const tomasSmith: ReadonlyEmployeeData = {
  specialization: "Administration",
  yearsOfExperience: 25,

  firstName: "Tomas",
  lastName: "Smith",
  gender: "male",
  age: 55,
};
