import { ReadonlyStudentData } from "../../types/persons-types/personsTypes";

export const lanaKicks: ReadonlyStudentData = {
  coveringLetter: true,
  participationInTheOlympiads: true,
  overallExamsScore: 96,

  firstName: "Lana",
  lastName: "Kicks",
  gender: "female",
  age: 18,
};

export const emilyStack: ReadonlyStudentData = {
  coveringLetter: true,
  participationInTheOlympiads: false,
  overallExamsScore: 80,

  firstName: "Emily",
  lastName: "Stack",
  gender: "female",
  age: 20,
};

export const charlieBool: ReadonlyStudentData = {
  coveringLetter: true,
  participationInTheOlympiads: true,
  overallExamsScore: 30,

  firstName: "Charlie",
  lastName: "Bool",
  gender: "male",
  age: 18,
};

export const josephLoop: ReadonlyStudentData = {
  coveringLetter: true,
  participationInTheOlympiads: true,
  overallExamsScore: 100,

  firstName: "Joseph",
  lastName: "Loop",
  gender: "male",
  age: 19,
};
