import { ReadonlyEmployeeData } from "../../types/persons-types/personsTypes";

export const oliverWilliams: ReadonlyEmployeeData = {
  specialization: "Astronomy",
  yearsOfExperience: 20,

  firstName: "Oliver",
  lastName: "Williams",
  gender: "male",
  age: 46,
};

export const helenaRichards: ReadonlyEmployeeData = {
  specialization: "History",
  yearsOfExperience: 7,

  firstName: "Helena",
  lastName: "Richards",
  gender: "female",
  age: 32,
};

export const haroldErickson: ReadonlyEmployeeData = {
  specialization: "Archeology",
  yearsOfExperience: 35,

  firstName: "Harold",
  lastName: "Erickson",
  gender: "male",
  age: 65,
};

export const semKholodov: ReadonlyEmployeeData = {
  specialization: "Law",
  yearsOfExperience: 12,

  firstName: "Sem",
  lastName: "Kholodov",
  gender: "male",
  age: 40,
};
