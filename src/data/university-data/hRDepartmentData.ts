import { ReadOnlyDynamicObject } from "../../types/common-types/commonTypes";

import { Role } from "../../types/persons-types/personsTypes";

import { harvardUniversityCourses } from "./universityData";

export const specializationsByRole: ReadOnlyDynamicObject<string[]> = {
  [Role.Director]: ["administration"],
  [Role.Teacher]: harvardUniversityCourses,
};

export const minYearsExperienceForRole: ReadOnlyDynamicObject<number> = {
  [Role.Director]: 20,
  [Role.Teacher]: 10,
};

export const minSalaryForRole: ReadOnlyDynamicObject<number> = {
  [Role.Director]: 3400,
  [Role.Teacher]: 2200,
};
