import { IUniversityData } from "../../types/university-types/universityTypes";

export const harvardUniversityCourses: string[] = [
  "history",
  "music",
  "chemistry",
  "physics",
  "psychiatry",
  "oncology",
  "law",
  "archeology",
  "astronomy",
];

export const harvardUniversity: IUniversityData = {
  name: "Harvard",
  courses: harvardUniversityCourses,
  location: {
    country: "USA",
    city: "Cambridge, MA",
    address: "5 James Street",
  },
};
