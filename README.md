# 05_typescript

This is a project created to mimic the work of a university. It involves TypeScript development.

## Installation

#### To use this project, follow the instructions below:

- Clone the repository to your local machine. `git clone [repository_url]`

- Navigate to the project directory. `cd 05_typescript`

- Install the required dependencies using `npm install`.

## Usage

#### Build

To build the project, run the following command: `npm run build`.

This command will compile the TypeScript source files into JavaScript and store them in the dist directory.

#### Start

To start the project, run the following command: `npm run start`.

This command will build the project and then execute the compiled index.js file located in the dist directory.

Start in Development Mode
If you prefer to use development mode with automatic restarts on file changes, you can run the following command: `npm run start:dev`.

This command uses nodemon and ts-node to automatically rebuild and restart the project whenever changes are detected in typescript files.

## Dependencies

#### The project has the following dependencies:

- uuid: A library for generating and working with Universally Unique Identifiers (UUIDs). Version 9.0.0 is used.

#### Dev Dependencies

The project has the following development dependencies:

- @types/uuid: TypeScript type definitions for the uuid library. Version 9.0.1 is used.

- nodemon: A development tool that automatically restarts the Node.js application when file changes are detected. Version 2.0.22 is used.

- ts-node: A TypeScript execution environment for Node.js. Version 10.9.1 is used.

#### Usage Notes

- In the index.ts file of this project, several methods and properties are used with console.log. These are used to display output in the console for various purposes, such as debugging or displaying information to the user during program execution.
